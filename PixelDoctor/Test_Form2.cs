﻿using System.Windows.Forms;

namespace PixelDoctor
{
    public partial class Test_Form2 : Form
    {
        int xoldpozisyon, yoldpozisyon;

        public Test_Form2()
        {
            InitializeComponent();
        }

        private void Test_Form2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Close();
        }

        private void Test_Form2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button.ToString() == "None")
            {
                xoldpozisyon = MousePosition.X;
                yoldpozisyon = MousePosition.Y;
            }
            if (e.Button.ToString() == "Left")
            {
                SetDesktopLocation(MousePosition.X - xoldpozisyon, MousePosition.Y - yoldpozisyon);
            }

        }

        private void Test_Form2_MouseDown(object sender, MouseEventArgs e)
        {
            xoldpozisyon = e.X;
            yoldpozisyon = e.Y;

        }
    }
}
