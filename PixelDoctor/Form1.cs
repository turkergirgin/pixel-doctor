﻿using System;
using System.Windows.Forms;

namespace PixelDoctor
{
    public partial class Form1 : Form
    {
        int say = 0;
        int sinir;
        int hangisi = 1;

        Test_Form Test_Form = new Test_Form();
        Test_Form2 Test_Form2 = new Test_Form2();
        About About = new About();

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Cursor.Hide();
            if (radioButton1.Checked) Test_Form.BackColor = radioButton1.BackColor;
            else if (radioButton2.Checked) Test_Form.BackColor = radioButton2.BackColor;
            else if (radioButton3.Checked) Test_Form.BackColor = radioButton3.BackColor;
            else if (radioButton4.Checked) Test_Form.BackColor = radioButton4.BackColor;
            else if (radioButton5.Checked) Test_Form.BackColor = radioButton5.BackColor;
            else if (radioButton6.Checked) Test_Form.BackColor = radioButton6.BackColor;
            else
            {
                Test_Form.BackColor = radioButton7.BackColor;
            }
            Test_Form.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            radioButton7.Checked = true;
            colorDialog1.ShowDialog();
            radioButton7.BackColor = colorDialog1.Color;
            radioButton7.Text = radioButton7.BackColor.Name;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            sinir = 6; hangisi = 1;
            timer1.Interval = (int)numericUpDown1.Value * 1000;
            timer1.Enabled = true;
            timer1_Tick(sender,e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (say > sinir) say = 0;
            say++;
            if (hangisi == 1)
            {
                if (say == 1) Test_Form.BackColor = radioButton1.BackColor;
                if (say == 2) Test_Form.BackColor = radioButton2.BackColor;
                if (say == 3) Test_Form.BackColor = radioButton3.BackColor;
                if (say == 4) Test_Form.BackColor = radioButton4.BackColor;
                if (say == 5) Test_Form.BackColor = radioButton5.BackColor;
                if (say == 6) Test_Form.BackColor = radioButton6.BackColor;
                if (say == 7) Test_Form.BackColor = radioButton7.BackColor;
            }
            else
            {
                if (say == 1) Test_Form2.BackColor = radioButton1.BackColor;
                if (say == 2) Test_Form2.BackColor = radioButton2.BackColor;
                if (say == 3) Test_Form2.BackColor = radioButton3.BackColor;
                if (say == 4) Test_Form2.BackColor = radioButton4.BackColor;
                if (say == 5) Test_Form2.BackColor = radioButton5.BackColor;
                if (say == 6) Test_Form2.BackColor = radioButton6.BackColor;
                if (say == 7) Test_Form2.BackColor = radioButton7.BackColor;
            }

            //MessageBox.Show(Convert.ToString(sender));
            if (hangisi == 2)
            {
                if (Test_Form2.Visible == false)
                {
                    //Test_Form2 Test_Form2 = new Test_Form2();
                    Test_Form2.ShowDialog();
                }
            }
            else if (Test_Form.Visible == false) Test_Form.ShowDialog();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Test_Form.piksel = 0;
            Test_Form.WindowState = FormWindowState.Maximized;
            sinir = 5; hangisi = 1;
            timer1.Interval = (int)numericUpDown2.Value;
            timer1.Enabled = true;
            timer1_Tick(sender, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Test_Form2 Test_Form2 = new Test_Form2();
            Test_Form2.Height = 1;
            Test_Form2.Width = 1;
            Test_Form2.WindowState = FormWindowState.Normal;
            sinir = 5; hangisi = 2;
            timer1.Interval = (int)numericUpDown2.Value;
            timer1.Enabled = true;
            timer1_Tick(sender, e);

        }

        private void button6_Click(object sender, EventArgs e)
        {
            About.ShowDialog();
        }


    }
}
