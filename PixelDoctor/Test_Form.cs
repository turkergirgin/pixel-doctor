﻿using System;
using System.Windows.Forms;

namespace PixelDoctor
{
    public partial class Test_Form : Form
    {
        public int piksel;

        public Test_Form()
        {
            InitializeComponent();
        }

        private void Test_Form_MouseClick(object sender, MouseEventArgs e)
        {
            Close();
        }

        private void Test_Form_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void Test_Form_VisibleChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(Convert.ToString(piksel));
            if (Visible == true && piksel == 0)
            {
                Cursor.Hide();
            }
            else
            {
                Cursor.Show();
            }
        }

    }
}
